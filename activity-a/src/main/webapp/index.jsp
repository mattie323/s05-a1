<!-- Imports in JSP can be achieved by using directives -->
<!-- Directives are defined by the "%@" symbol which is used to define page attributes -->
<!-- Multiple import packages can be achieved  by adding a comma (e.g import = "java.util.data",java.util.* -->
<%@ page language="java" 
	contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import= "java.util.*,java.text.*"
    %>
    
   
  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Java Server Pages</title>
</head>
<body>
	<h1>Our Date and Time now is...</h1>
	
	<!-- JSP allows integration of HTML tags with java syntax -->
	
	<!-- Prints a message in the console -->
	<% System.out.println("Hello from JSP"); %>
	<!-- Creates a variable currentDateTime -->
	<%! 
		Date currentDateTime = new Date();
	
		
	%> 
	<%
	// Get the current date and time
	Date now = new Date();
	//This method from an object DateFormat which is getTimeInstace() is used to get the time format
	DateFormat dateFormat = DateFormat.getTimeInstance();
	//setTimZone method for Manila			continent  captial of a country
	dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Manila"));
	String manila = dateFormat.format(now);
	//setTimZone method for Japan			continent captial of a country
	dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Tokyo"));
	String japan = dateFormat.format(now);
	//setTimZone method for Germany			 continent  captial of a country
	dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
	String germany = dateFormat.format(now);
	//this method is used for changing the format of the date
	dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	String formatDate = dateFormat.format(new Date());
			
	%>
	<!--The ul defines unordered list and it usually displayed bullet(s) and li is the list items that will be put inside to a ul  -->
	 <ul>
		  <li>Manila: <%= formatDate+" "+manila %></li>
		  <li>Japan: <%= formatDate+" "+japan %></li>
		  <li>Germany: <%= formatDate+" "+germany %></li>
	</ul> 
	<!-- JSP declaration -->
	<!-- Allows declaration one or more variables and methods -->
	<%!
		private int initVar=0;
		private int serviceVar=0;
		private int destroyVar=0;
	%>
	<!-- JSP method declaration -->
	<%!
		public void jspInit(){
			initVar++;
			System.out.println("jspInit(): init"+initVar);
	}
		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspDestroy(): destroy"+destroyVar);
		}
	%>
	<!-- JSP Scriplets -->
	<!-- Allows any java language statement, variable, method declaration and expression -->
	<%
		serviceVar++;
		System.out.println("jspService(): service"+serviceVar);
		String content1 = "initialization(content1): "+initVar;
		String content2 = "service(content2): "+serviceVar;
		String content3 = "destroy(content3): "+destroyVar;
		
	%>
	<!-- JSP expression -->
	<!-- Code placed within the JSP expression tag is written to the output stream of the response -->
	<!-- out.println is no longer required to print values of variables/methods -->
	<h1>JSP Expressions</h1>
	<p><%= content1	%></p>
	<p><%= content2%></p>
	<p><%= content3%></p>
	
	<!-- <h1>Create an account</h1>
	<form action="user" method ="post">
	
		<label for="firstName">First Name: </label>
		<input type="text" name="firstName"><br>
		
		<label for="lastName">Last Name: </label>
		<input type="text" name="lastName"><br>
		
		<label for="email">Email: </label>
		<input type="email" name="email"><br>
		
		<label for="contact">Contact: </label>
		<input type="text" name="contact"><br>
		<input type="submit">
	</form> -->
</body>
</html>