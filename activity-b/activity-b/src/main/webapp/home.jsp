<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
</head>
<body>
	 <%
	String fName = session.getAttribute("firstName").toString();
	String lName = session.getAttribute("lastName").toString();
	String userType = session.getAttribute("userType").toString();
	%> 
	<h1>Welcome <%=" "+fName+" "+lName %>!</h1>
	
	<%
		if(userType.equals("Applicant")){
			out.println("<p>Welcome "+userType+". You may now start looking for your career opportunity.</p>");
		}
		else if(userType.equals("Employer")){
			out.println("<p>Welcome "+userType+". You may now start browsing applicant profiles.</p>");
		}
	%>
</body>
</html>