<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Servlet Job Finder</title>
	<style>
		div {
			margin-top: 5px;
			margin-bottom: 5px;
		}
		</style>
</head>
<body>
	<h1>Welcome to Servlet Job Finder!</h1>
	<form method="post" action="register">
		<div>
			<label for="firstName">First Name</label>
			<input type="text" name="firstName" required>
		</div>
		
		<div>
			<label for="lastName">Last Name</label>
			<input type="text" name="lastName" required>
		</div>
		
		<div>
			<label for="phone">Phone</label>
			<input type="tel" name="phone" required>
		</div>
		
		<div>
			<label for="email">Email</label>
			<input type="email" name="email" required>
		</div>
		
		<fieldset>
			<legend>How did you discover the app?</legend>
			<input type="radio" id="friends" name="discovery" required value="Friends">
			<label for="friends">Friends</label>
			<br>
			
			<input type="radio" id="socialmedia" name="discovery" required value="Social Media">
			<label for="four_seater">Social Media</label>
			<br>
			
			<input type="radio" id="Others" name="discovery" required value="Others">
			<label for="six_seater">Others</label>
			<br>
		</fieldset>
		
		<div>
			<label for="dateBirth">Date of Birth</label>
			<input type="date" name="dateBirth" required>
		</div>
		
		<div>
			<label for="userType">Are you an Employer or Applicant?</label>
			<select id="userType" name="userType">
				<option value="" selected="selected">Select One</option>
				<option value="Employer">Employer</option>
				<option value="Applicant">Applicant</option>
			</select>
		</div>
		<div>
			<label for="profileDescription">Profile Description</label>
			<textarea name="profileDescription" maxlength="500"></textarea>
		</div>
		<button>Register</button>
	</form>
</body>
</html>