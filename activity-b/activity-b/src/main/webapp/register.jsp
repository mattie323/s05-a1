<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registration Confirmation</title>
</head>
<body>
	<h1>Registration Confirmation</h1>
	<p>First Name: <%= session.getAttribute("firstName") %></p>
	<p>Last Name: <%= session.getAttribute("lastName") %></p>
	<p>Phone: <%= session.getAttribute("phone") %></p>
	<p>Email: <%= session.getAttribute("email") %></p>
	<p>App Discovery: <%= session.getAttribute("discover") %></p>
	<p>Date of Birth: <%= session.getAttribute("dateBirth") %></p>
	<p>User Type: <%= session.getAttribute("userType") %></p>
	<p>Description: <%= session.getAttribute("profileDescription") %></p>
	
	<form method="post" action = "login">
        <input type = "submit" value = "Submit">
	</form>
	<form action = "index.jsp">
        <input type = "submit" value = "Back">
	</form>
</body>
</html>