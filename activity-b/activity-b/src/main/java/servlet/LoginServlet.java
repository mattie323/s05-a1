package servlet;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	
	public void init() throws ServletException {
		System.out.println("LoginServlet has been initialized.");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		

		HttpSession session = req.getSession();
		session.getAttribute("firstName");
		session.getAttribute("lastName");
		session.getAttribute("userType");
		res.sendRedirect("home.jsp");
	}
	public void destroy() {
		System.out.println("LoginServlet has been finalized.");
	}
}
