package servlet;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	
	public void init() throws ServletException {
		System.out.println("RegisterServlet has been initialized.");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String fName = req.getParameter("firstName");
		String lName = req.getParameter("lastName");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String discover = req.getParameter("discovery");
		String dBirth = req.getParameter("dateBirth");
		String userType = req.getParameter("userType");
		String pDescription = req.getParameter("profileDescription");
		//Stores all the data from the form into the session
		HttpSession session=req.getSession();
		session.setAttribute("firstName",fName);
		session.setAttribute("lastName",lName);
		session.setAttribute("phone",phone);
		session.setAttribute("email",email);
		session.setAttribute("discover",discover);
		session.setAttribute("dateBirth",dBirth);
		session.setAttribute("userType",userType);
		session.setAttribute("profileDescription",pDescription);
		res.sendRedirect("register.jsp");
		
	}
	public void destroy() {
		System.out.println("RegisterServlet has been destroyed.");
	}
}
